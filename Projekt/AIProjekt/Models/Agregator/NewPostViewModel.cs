﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using AIProjekt.Domain.CustomAttributes;

namespace AIProjekt.Models.Agregator
{
    public class NewPostViewModel
    {
        [DisplayName("Tytuł")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} jest wymagany")]
        [MaxLength(100, ErrorMessage = "{0} może mieć maksymalnie 100 znaków.")]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} jest wymagany")]
        [MaxLength(200, ErrorMessage = "{0} może mieć maksymalnie 200 znaków.")]
        [DisplayName("Opis")]
        public string Description { get; set; }

        [DisplayName("Obrazek")]
        [ValidateFileSize(ErrorMessage = "Dozwolony rozmiar < 1MB")]
        [ValidateFileExtension(ErrorMessage = "Wymagany format: *.png , *.bmp, *.jpeg")]
        public HttpPostedFileBase File { get; set; }

        [DisplayName("Link do treści")]
        [LinkValidator(ErrorMessage = "Nieprawidłowy adres URL.")]
        public string Link { get; set; }

        [DisplayName("Treść")]
        public string Contents { get; set; }
    }
}