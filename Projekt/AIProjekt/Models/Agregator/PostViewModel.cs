﻿using System;
using AIProjekt.Domain.DbModels;

namespace AIProjekt.Models.Agregator
{
    public class PostViewModel
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime TimeAdded { get; set; }
        public string AuthorId { get; set; }
        public string Author { get; set; }
        public int CommentCount { get; set; }

        public int PostiveRank { get; set; }
        public int NegativeRank { get; set; }

        public bool HasImage { get; set; }
        public string ImageName { get; set; }

        public Digged? Digged { get; set; }
    
    }
}