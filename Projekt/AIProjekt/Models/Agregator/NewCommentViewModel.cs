﻿using System.ComponentModel.DataAnnotations;

namespace AIProjekt.Models.Agregator
{
    public class NewCommentViewModel
    {
        [Required]
        public int PostId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Komentarz nie może być pusty!")]
        public string Contents { get; set; }
    }
}