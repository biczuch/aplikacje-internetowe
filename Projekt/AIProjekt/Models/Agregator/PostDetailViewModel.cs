﻿using System.Collections.Generic;
using AIProjekt.Domain.DbModels;

namespace AIProjekt.Models.Agregator
{
    public class PostDetailViewModel : PostViewModel
    {
        public IEnumerable<Comment> Comments { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }

        public NewCommentViewModel NewComment { get; set; }
    }
}