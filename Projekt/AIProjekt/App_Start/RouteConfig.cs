﻿using System.Web.Mvc;
using System.Web.Routing;

namespace AIProjekt
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                url: "strona/{page}",
                defaults: new { controller = "Agregator", action = "Index", page = UrlParameter.Optional },
                name: "AgregatorIndex"
            );

            routes.MapRoute(
                url: "nowyPost",
                defaults: new { controller = "Post", action = "NewPost" },
                name: "NewPost"
            );

            routes.MapRoute(
                url: "post/{postId}",
                defaults: new { controller = "Post", action = "Index", postId = "postId" },
                name: "PostDetails"
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Agregator", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
