﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AIProjekt.Startup))]
namespace AIProjekt
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
