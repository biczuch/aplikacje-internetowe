﻿using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace AIProjekt.Domain.HtmlExtenstions
{
    public static class YoutubeLinkValidator
    {
        public static bool IsValidYoutubeAddress(this HtmlHelper helper, string youtubeLink)
        {
            if (youtubeLink == null)
            return false;

            const string pattern =
                @"(?:.+?)?(?:\/v\/|watch\/|\?v=|\&v=|youtu\.be\/|\/v=|^youtu\.be\/)([a-zA-Z0-9_-]{11})+";

            return Regex.IsMatch(youtubeLink, pattern);
        }

        public static string GetVideoIdFromYoutubeLink(this HtmlHelper helper, string youtubeLink)
        {
            const string pattern =
                @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
            var x = Regex.Match(youtubeLink, pattern).Groups[1].Value;
            return x;
        }
    }
}