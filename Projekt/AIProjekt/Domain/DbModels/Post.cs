﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AIProjekt.Domain.DbModels
{
    public class Post
    {
        public Post()
        {
            Comments = new HashSet<Comment>();
            PostUsers = new HashSet<PostUser>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [MaxLength(200)]
        public string Description { get; set; }

        public DateTime TimeAdded { get; set; }

        public int Rank { get; set; }

        public byte[] Image { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }

        public string Link { get; set; }
        public string Contents { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<PostUser> PostUsers { get; set; }
    }
}