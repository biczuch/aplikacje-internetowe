﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AIProjekt.Domain.DbModels
{
    public class PostUser
    {
        [Key, ForeignKey("Post")]
        [Column(Order = 0)]
        public int PostId { get; set; }
        public virtual Post Post { get; set; }

        [Key, ForeignKey("User")]
        [Column(Order = 1)]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        public Digged? Digged { get; set; }
    }

    public enum Digged
    {
        Buried,
        Digged
    }
}