﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AIProjekt.Domain.DbModels
{
    public class User : IdentityUser
    {
        public User()
        {
            Comments = new HashSet<Comment>();
            Posts = new HashSet<Post>();
            PostUsers = new HashSet<PostUser>();
        }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<PostUser> PostUsers { get; set; }
    }
}