﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AIProjekt.Domain.DbModels
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime CommentDate { get; set; }
        [Required]
        public string Contents { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}