﻿using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace AIProjekt.Domain.CustomAttributes
{
    public class ValidateFileExtensionAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            HttpPostedFileBase file = value as HttpPostedFileBase;

            if (file == null)
                return true;

            try
            {
                using (var image = Image.FromStream(file.InputStream))
                {
                    return (image.RawFormat.Equals(ImageFormat.Png) ||
                        image.RawFormat.Equals(ImageFormat.Bmp) ||
                        image.RawFormat.Equals(ImageFormat.Jpeg));
                }
            }
            catch
            {
                return false;
            }
        }
    }
}