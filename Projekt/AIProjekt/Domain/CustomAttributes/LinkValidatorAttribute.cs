﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AIProjekt.Domain.CustomAttributes
{
    public class LinkValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string url = value as string;

            if (url == null)
                return true;

            Uri uri;
            if (!(Uri.TryCreate(url, UriKind.Absolute, out uri)
                  && (uri.Scheme == Uri.UriSchemeHttp
                      || uri.Scheme == Uri.UriSchemeHttps
                      || uri.Scheme == Uri.UriSchemeFtp)))
            {
                return false;
            }

            return true;
        }
    }
}