﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AIProjekt.Domain.CustomAttributes
{
    public class ValidateFileSizeAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            HttpPostedFileBase file = value as HttpPostedFileBase;

            if (file == null)
                return true;

            if (file.ContentLength > 1 * 1024 * 1024)
                return false;

            return true;
        }
    }
}