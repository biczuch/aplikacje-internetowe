﻿using System.Data.Entity;
using AIProjekt.Domain.DbModels;
using AIProjekt.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AIProjekt.Domain
{
    public class ZakopContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<PostUser> PostUsers { get; set; }
        public DbSet<User> Users { get; set; }

        public ZakopContext()
            : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey<string>(r => r.Id).ToTable("AspNetUsers");
            modelBuilder.Entity<IdentityUserLogin>().HasKey(k => new { k.LoginProvider, k.ProviderKey, k.UserId }).ToTable("AspNetUserLogins");
            modelBuilder.Entity<IdentityUserRole>().HasKey(k => new { k.UserId, k.RoleId }).ToTable("AspNetUserRoles");
            modelBuilder.Entity<IdentityUserClaim>().HasKey(k => new { k.Id }).ToTable("AspNetUserClaims");

            modelBuilder.Entity<PostUser>().HasKey(pu => new { pu.PostId, pu.UserId });



            base.OnModelCreating(modelBuilder);
        }
    }
}