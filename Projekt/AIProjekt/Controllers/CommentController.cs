﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AIProjekt.Domain;
using AIProjekt.Domain.DbModels;
using System.Data.Entity;
using AIProjekt.Models.Agregator;
using Microsoft.AspNet.Identity;

namespace AIProjekt.Controllers
{
    public class CommentController : Controller
    {
        [Authorize]
        [HttpPost]
        public ActionResult AddComment(NewCommentViewModel newComment)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return Json(allErrors);
            }

            using (ZakopContext context = new ZakopContext())
            {
                Post post = context.Posts.Include(p => p.Comments).FirstOrDefault(p => p.Id == newComment.PostId);


                if (post == null)
                    return View("Error");

                string userId = User.Identity.GetUserId();
                User user = context.Users.FirstOrDefault(u => u.Id == userId);

                Comment comment = new Comment
                {
                    CommentDate = DateTime.Now,
                    Contents = newComment.Contents,
                    User = user,
                    Post = post
                };

                post.Comments.Add(comment);

                context.SaveChanges();

                IEnumerable<Comment> comments = post.Comments;

                return PartialView("CommentsPartial", comments);
            }
        }
    }
}