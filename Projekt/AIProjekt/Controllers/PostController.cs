﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Linq;
using System.Data.Entity;
using AIProjekt.Domain;
using AIProjekt.Domain.DbModels;
using AIProjekt.Models.Agregator;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;

namespace AIProjekt.Controllers
{
    public class PostController : Controller
    {
        public ActionResult Index(int postId)
        {
            using (ZakopContext context = new ZakopContext())
            {
                string userId = User.Identity.GetUserId();

                PostDetailViewModel postDetails = (from p in context.Posts
                                                   where p.Id == postId
                                                   select new PostDetailViewModel
                                                   {
                                                       PostId = p.Id,
                                                       Title = p.Title,
                                                       Description = p.Description,
                                                       Content = p.Contents,
                                                       Link = p.Link,
                                                       AuthorId = p.UserId,
                                                       Author = p.User.UserName,
                                                       TimeAdded = p.TimeAdded,
                                                       CommentCount = p.Comments.Count,
                                                       ImageName = p.FileName,
                                                       HasImage = p.Image != null,
                                                       Digged = p.PostUsers.FirstOrDefault(pu => pu.UserId == userId).Digged,
                                                       PostiveRank = context.PostUsers
                                                                     .Count(pu => pu.PostId == p.Id && pu.Digged == Digged.Digged),
                                                       NegativeRank = context.PostUsers
                                                              .Count(pu => pu.PostId == p.Id && pu.Digged == Digged.Buried)
                                                   })
                    .SingleOrDefault();

                if (postDetails == null)
                    return View("Error");

                postDetails.Comments =
                    context.Comments.Include(c => c.User)
                        .Where(c => c.PostId == postDetails.PostId)
                        .OrderByDescending(c => c.CommentDate)
                        .ToArray();

                if (!postDetails.Content.IsNullOrWhiteSpace())
                    postDetails.Content = postDetails.Content.Replace("\n", "<br />");

                return View("PostDetails", postDetails);
            }
        }


        public ActionResult NewPost()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult NewPost(NewPostViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (model.Contents.IsNullOrWhiteSpace() && model.Link.IsNullOrWhiteSpace())
                ModelState.AddModelError("NullContents", "Link lub Zawartość nie może być pusta.");

            if (model.File != null && model.File.ContentLength > 0)
            {
                try
                {
                    Image.FromStream(model.File.InputStream);
                    model.File.InputStream.Position = 0;
                    //valid input
                }
                catch
                {
                    ModelState.AddModelError("Image", "Nieprawidłowy format obrazka.");
                }
            }

            if (!ModelState.IsValid)
                return View(model);

            Post post = new Post
            {
                Title = model.Title,
                Description = model.Description,
                Contents = model.Contents,
                Link = model.Link,
                TimeAdded = DateTime.Now,
                Rank = 0
            };

            if (model.File != null && model.File.ContentLength > 0)
            {
                post.FileName = model.File.FileName;
                post.ContentType = model.File.ContentType;

                byte[] fileContent = new byte[model.File.ContentLength];
                model.File.InputStream.Read(fileContent, 0, model.File.ContentLength);

                post.Image = fileContent;
            }

            string userId = User.Identity.GetUserId();

            using (ZakopContext context = new ZakopContext())
            {
                User user = context.Users.FirstOrDefault(u => u.Id == userId);
                post.User = user;
                context.Posts.Add(post);
                context.SaveChanges();
            }

            return RedirectToAction("Index", "Post", new { postId = post.Id });
        }
    }
}