﻿using System.Web.Mvc;

namespace AIProjekt.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult About()
        {
            return View();
        }
    }
}