﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Data.Entity;
using AIProjekt.Domain;
using AIProjekt.Domain.DbModels;
using AIProjekt.Models.Agregator;
using Microsoft.AspNet.Identity;
using PagedList;

namespace AIProjekt.Controllers
{
    public class AgregatorController : Controller
    {
        public ActionResult Index(int page = 1)
        {
            const int pageSize = 2;

            using (ZakopContext context = new ZakopContext())
            {
                var posts = GetPostsFromDb(context);

                posts = posts.Where(p => p.PostiveRank > 1 && (float)p.PostiveRank / (p.NegativeRank + 1) > 0.25);

                PagedList<PostViewModel> model = new PagedList<PostViewModel>(posts, page, pageSize);

                return View(model);
            }
        }

        public ActionResult RisingIndex(int page = 1)
        {
            const int pageSize = 2;

            using (ZakopContext context = new ZakopContext())
            {
                var posts = GetPostsFromDb(context);

                posts = posts.Where(p => p.PostiveRank <= 1 || (float)p.PostiveRank / (p.NegativeRank + 1) <= 0.25);

                PagedList<PostViewModel> model = new PagedList<PostViewModel>(posts, page, pageSize);

                return View(model);
            }
        }

        private IEnumerable<PostViewModel> GetPostsFromDb(ZakopContext context)
        {
            string userId = User.Identity.GetUserId();
            IEnumerable<PostViewModel> posts = (from Post p in context.Posts
                .Include(p => p.User)
                .Include(p => p.Comments)
                .Include(p => p.PostUsers)
                orderby p.TimeAdded descending
                select new PostViewModel
                {
                    PostId = p.Id,
                    AuthorId = p.UserId,
                    Author = p.User.UserName,
                    PostiveRank = context.PostUsers
                        .Count(pu => pu.PostId == p.Id && pu.Digged == Digged.Digged),
                    NegativeRank = context.PostUsers
                        .Count(pu => pu.PostId == p.Id && pu.Digged == Digged.Buried)
                    ,
                    Digged = p.PostUsers
                        .Where(pu => pu.PostId == p.Id && pu.UserId == userId)
                        .Select(pu => pu.Digged)
                        .FirstOrDefault(),
                    CommentCount = p.Comments.Count,
                    Title = p.Title,
                    TimeAdded = p.TimeAdded,
                    Description = p.Description,
                    HasImage = p.Image != null
                }).ToList();
            return posts;
        }


        [Authorize]
        public ActionResult Digg(int postId)
        {
            using (ZakopContext context = new ZakopContext())
            {
                Post post = context.Posts.Include(p => p.PostUsers).SingleOrDefault(p => p.Id == postId);
                string userId = User.Identity.GetUserId();
                User user = context.Users.SingleOrDefault(u => u.Id == userId);

                if (post == null || post.UserId == user.Id)
                {
                    if (Request.IsAjaxRequest())
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    return View("Error");
                }

                PostUser postUser = post.PostUsers.SingleOrDefault(pu => pu.UserId == user.Id && pu.PostId == post.Id);

                if (postUser != null)
                {
                    switch (postUser.Digged)
                    {
                        case Digged.Digged:
                            postUser.Digged = null;
                            break;
                        case Digged.Buried:
                            postUser.Digged = Digged.Digged;
                            break;
                        default:
                            postUser.Digged = Digged.Digged;
                            break;
                    }
                }
                else
                {
                    postUser = new PostUser
                    {
                        Digged = Digged.Digged,
                        User = user,
                        Post = post
                    };

                    post.PostUsers.Add(postUser);
                }

                context.SaveChanges();

                if (Request.IsAjaxRequest())
                {
                    var dataToSend = new
                    {
                        postId = post.Id,
                        digged = postUser.Digged,
                        positiveRank = post.PostUsers.Count(pu => pu.Digged == Digged.Digged),
                        negativeRank = post.PostUsers.Count(pu => pu.Digged == Digged.Buried),
                    };
                    return Json(dataToSend, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index", "Post", new { postId = post.Id });
            }
        }


        [Authorize]
        public ActionResult Bury(int postId)
        {
            using (ZakopContext context = new ZakopContext())
            {
                Post post = context.Posts.Include(p => p.PostUsers)
                    .Include(p => p.PostUsers)
                    .SingleOrDefault(p => p.Id == postId);
                string userId = User.Identity.GetUserId();
                User user = context.Users.SingleOrDefault(u => u.Id == userId);

                if (post == null || post.UserId == user.Id)
                {
                    if (Request.IsAjaxRequest())
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    return View("Error");
                }

                PostUser postUser = post.PostUsers.SingleOrDefault(pu => pu.UserId == user.Id && pu.PostId == post.Id);

                if (postUser != null)
                {
                    switch (postUser.Digged)
                    {
                        case Digged.Digged:
                            postUser.Digged = Digged.Buried;
                            break;
                        case Digged.Buried:
                            postUser.Digged = null;
                            break;
                        default:
                            postUser.Digged = Digged.Buried;
                            break;
                    }
                }
                else
                {
                    postUser = new PostUser
                    {
                        Digged = Digged.Buried,
                        User = user,
                        Post = post
                    };

                    post.PostUsers.Add(postUser);
                }

                context.SaveChanges();

                if (Request.IsAjaxRequest())
                {
                    var dataToSend = new
                    {
                        postId = post.Id,
                        digged = postUser.Digged,
                        positiveRank = post.PostUsers.Count(pu => pu.Digged == Digged.Digged),
                        negativeRank = post.PostUsers.Count(pu => pu.Digged == Digged.Buried),
                    };
                    return Json(dataToSend, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index", "Post", new { postId = post.Id });
            }
        }

        public FileContentResult GetImage(int postId)
        {
            using (ZakopContext context = new ZakopContext())
            {
                Post post = context.Posts.FirstOrDefault(p => p.Id == postId);
                if (post != null)
                {
                    return new FileContentResult(post.Image, post.ContentType);
                }
                return null;
            }
        }
    }
}