﻿function updateRank(data) {
    var rank = data.positiveRank - data.negativeRank;

    var rankDiv = $('#' + data.postId);
    $(".positive-rank", rankDiv).text(data.positiveRank);
    $(".negative-rank", rankDiv).text(data.negativeRank);
    $(".cumulated-rank", rankDiv).text(rank);


    $('.btn', rankDiv).removeClass('btn-default');
    $('.dig', rankDiv).addClass('btn-primary');
    $('.bury', rankDiv).addClass('btn-danger');

    var digged = data.digged;
    //buried
    if (digged === 0) {
        var button = $('.bury', rankDiv);
        button.removeClass('btn-danger');
        button.addClass('btn-default');
    }
        //digged
    else if (digged === 1) {
        var button = $('.dig', rankDiv);
        button.removeClass('btn-primary');
        button.addClass('btn-default');
    }
}