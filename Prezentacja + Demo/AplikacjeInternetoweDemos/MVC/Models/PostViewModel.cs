﻿using System.Collections.Generic;

namespace MVC.Models
{
    public class PostViewModel
    {
        public IEnumerable<Post> Posts { get; set; }
        public Post NewItem { get; set; }
    }
}