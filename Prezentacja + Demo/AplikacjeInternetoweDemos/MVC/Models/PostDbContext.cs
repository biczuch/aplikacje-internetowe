﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace MVC.Models
{
    public class MvcDemoContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
    }

    public class Post
    {
        [Key]
        public int PostId { get; set; }

        [Required(ErrorMessage = "Pole jest wymagane.")]
        public string Contents { get; set; }
    }

}