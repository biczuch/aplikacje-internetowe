﻿using System.Web.Mvc;

namespace MVC.Extensions
{
    public static class MyHtmlHelpers
    {
        public static MvcHtmlString BootstrapButton(this HtmlHelper helper, string text, string link)
        {
            TagBuilder anchor = new TagBuilder("a");
            anchor.AddCssClass("btn btn-primary");
            anchor.Attributes.Add("href", link);
            anchor.InnerHtml = text;
            return new MvcHtmlString(anchor.ToString());
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string path, string alt)
        {
            return new MvcHtmlString(string.Format("<img src='{0}' alt='{1}'>", path, alt));
        }
    }
}