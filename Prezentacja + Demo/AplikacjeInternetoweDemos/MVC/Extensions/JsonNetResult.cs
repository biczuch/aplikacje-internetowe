﻿using System;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace MVC.Extensions
{
    public class JsonNetResult : ActionResult
    {
        public object Data { get; set; }


        public JsonNetResult(object data)
        {
            Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = "application/json";

            if (Data != null)
            {
                JsonTextWriter writer = new JsonTextWriter(response.Output)
                {
                    Formatting = Formatting.Indented
                };

                JsonSerializer serializer = JsonSerializer.Create();
                string currentDate = DateTime.Now.Date.ToShortDateString();
                serializer.Serialize(writer, new {currentDate, Data});

                writer.Flush();
            }
        }
    }
}