﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MVC.CustomAttribute;
using MVC.Extensions;
using MVC.Models;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Items()
        {
            PostViewModel model = new PostViewModel
            {
                Posts = GetPostsFromDatabase()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Items(PostViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Posts = GetPostsFromDatabase();
                return View("Items", model);
            }

            AddPostToDatabase(model.NewItem);

            return RedirectToAction("Items");
        }

        [AjaxOnly]
        public ActionResult ItemsAjax(Post newItem)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Kurza twarz! Pole jest puste!");
            }

            AddPostToDatabase(newItem);

            IEnumerable<Post> postList = GetPostsFromDatabase();

            return PartialView("ItemListPartial", postList);
        }

        public ActionResult Json()
        {
            return new JsonNetResult("My text");
        }

        #region Private methods
        private IEnumerable<Post> GetPostsFromDatabase()
        {
            using (MvcDemoContext context = new MvcDemoContext())
            {
                return context.Posts.ToArray();
            }
        }

        private static void AddPostToDatabase(Post post)
        {
            using (MvcDemoContext context = new MvcDemoContext())
            {
                context.Posts.Add(post);
                context.SaveChanges();
            }
        }
        #endregion

    }
}