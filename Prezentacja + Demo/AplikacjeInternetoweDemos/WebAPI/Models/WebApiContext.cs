﻿using System.Data.Entity;

namespace WebAPI.Models
{
    public class WebApiContext : DbContext
    {
        public DbSet<Comment> Comments { get; set; }
    }

    public class Comment
    {
        public int CommentId { get; set; }
        public string Contents { get; set; }
    }
}