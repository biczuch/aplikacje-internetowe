﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class CommentsController : ApiController
    {
        // GET api/comments
        public IEnumerable<Comment> Get()
        {
            Comment[] comments;
            using (WebApiContext context = new WebApiContext())
            {
                comments = (from Comment p in context.Comments select p).ToArray();
            }
            return comments;
        }

        // GET api/comments/5
        public Comment Get(int id)
        {
            Comment comment;
            using (WebApiContext context = new WebApiContext())
            {
                comment = (from Comment p in context.Comments where p.CommentId == id select p).FirstOrDefault();
            }

            return comment;
        }

        // POST api/comments
        [HttpPost]
        public void Post(string contents)
        {
            using (WebApiContext context = new WebApiContext())
            {
                Comment comment = new Comment
                {
                    Contents = contents
                };
                context.Comments.Add(comment);
                context.SaveChanges();
            }
        }

        // PUT api/comments/5
        public void Put(int id, string contents)
        {
            using (WebApiContext context = new WebApiContext())
            {
                Comment comment = (from Comment p in context.Comments where p.CommentId == id select p).FirstOrDefault();
                if (comment != null)
                {
                    comment.Contents = contents;
                    context.SaveChanges();
                }      
            }
        }

        // DELETE api/comments/5
        public void Delete(int id)
        {
            using (WebApiContext context = new WebApiContext())
            {
                Comment comment = (from Comment p in context.Comments where p.CommentId == id select p).FirstOrDefault();
                if (comment != null)
                {
                    context.Comments.Remove(comment);
                    context.SaveChanges();
                }
            }
        }
    }
}
