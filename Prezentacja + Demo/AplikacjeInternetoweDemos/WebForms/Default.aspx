﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <p>Demo 1: Wyświetlanie aktualnej daty z serwera.</p>
    <asp:Button ID="DateButton" runat="server" Text="Aktualna data" OnClick="DateButton_Click" />
    <asp:TextBox ID="DateTextBox" runat="server"></asp:TextBox>
</asp:Content>

