﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                DateButton.BorderColor = Color.Blue;
            }
        }

        protected void DateButton_Click(object sender, EventArgs e)
        {
            DateTextBox.Text = DateTime.Now.ToShortDateString();
        }
    }
}