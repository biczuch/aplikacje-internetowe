﻿using System;
using System.Collections.Generic;
using System.Linq;
using EF.Entities;

namespace EF
{
    class Program
    {
        static void Main(string[] args)
        {
            using (EfDemoContext context = new EfDemoContext())
            {
                //Dodanie nowego postu
                Post post = context.Posts.Create();

                post.Date = DateTime.Now;
                post.Contents = "Bla bla bla bla";

                //Dodanie 10 komentarzy do postu
                for (int i = 0; i < 10; i++)
                {
                    Comment comment = new Comment
                    {
                        Contents = "Komentarz" + i,
                        Date = DateTime.Now.AddDays(1),
                    };
                    
                    post.Comments.Add(comment);
                }

                context.Posts.Add(post);
                context.SaveChanges();
            }

            using (EfDemoContext context = new EfDemoContext())
            {
                //Wyświetlenie postów wraz z komentarzami
                foreach (Post p in context.Posts)
                {
                    //Dodanie komentarzy do postu
                    context.Entry(p).Collection(c => c.Comments).Load();

                    Console.WriteLine(p.Date);
                    Console.WriteLine(p.Contents);
                    Console.WriteLine();

                    foreach (Comment comment in p.Comments)
                    {
                        Console.WriteLine("     " + comment.Date);
                        Console.WriteLine("     " + comment.Contents);
                    }
                    Console.WriteLine("-----------------------");
                }
                Console.ReadKey();
            }
        }
    }
}
