﻿using System.Data.Entity;
using EF.Entities;
namespace EF
{
    class EfDemoContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}
