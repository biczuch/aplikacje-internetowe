﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EF.Entities
{
    class Comment
    {
        [Key]
        public int CommentId { get; set; }

        public DateTime Date { get; set; }

        [MaxLength(100)]
        public string Contents { get; set; }

        public int PostId { get; set; }
        public virtual Post Post { get; set; }
    }
}
