﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EF.Entities
{
    class Post
    {
        public Post()
        {
            Comments = new List<Comment>();
        }

        [Key]
        public int PostId { get; set; }
    
        public DateTime Date { get; set; }

        public string Contents { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

    }
}
